import Ember from 'ember';

export default Ember.Controller.extend({
  filteredList: null,
  actions: {
    autoComplete(param) {
      if (param !== '') {
        Ember.$.getJSON("https://api.github.com/search/users?q=" + param + "&per_page=5").then((result) => this.set('filteredList', result.items));
      } else {
        this.set('filteredList', null);
      }
    },
    search(param) {
      if (param !== '') {
        Ember.$.getJSON("https://api.github.com/users/" + param + "/repos").then((result) => this.set('model', result));
        this.set('filteredList', null);
      } else {
        this.set('model', null);
      }
    }
  }
});
